##
# Project Title
#
# @file
# @version 0.1



# end

install: build run

build:
	./build.sh

run:
	docker run -p 8080:8080 registry.gitlab.com/z01d-b3rg/dropwizard-example:latest

create-ecs-cluster:
	scripts/create-ecs-cluster
