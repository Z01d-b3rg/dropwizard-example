module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a"]
  public_subnets  = ["10.0.101.0/24"]

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

module "ecs_cluster" {
  source = "infrablocks/ecs-cluster/aws"
  version = "4.0.0"

  region = "us-east-1"
  vpc_id = module.vpc.vpc_id
  subnet_ids = module.vpc.public_subnets
  allowed_cidrs = ["0.0.0.0/0"]

  component = "dropwizard"
  deployment_identifier = "production"

  cluster_name = "dropwizard"
  cluster_instance_type = "t2.micro"

  cluster_minimum_size = 1
  cluster_maximum_size = 1
  cluster_desired_capacity = 1
}

resource "aws_ecs_task_definition" "dropwizard" {
  family = "dropwizard"
  container_definitions = jsonencode([
    {
      name      = "app"
      image     = "registry.gitlab.com/z01d-b3rg/dropwizard-example:latest"
      cpu       = 200
      memory    = 200
      essential = true
      portMappings = [
        {
          containerPort = 8080
          hostPort      = 80
        }
      ]
    }
  ])
}

resource "aws_ecs_service" "dropwizard" {
  name            = "dropwizard"
  cluster         = module.ecs_cluster.cluster_id
  task_definition = aws_ecs_task_definition.dropwizard.arn
  desired_count   = 0
}
